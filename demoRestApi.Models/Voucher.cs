﻿using System;
using System.Collections.Generic;

namespace DemoRestApi.Models
{
    public partial class Voucher
    {
        public Voucher()
        {
            Orders = new HashSet<Order>();
        }

        public int VoucherId { get; set; }
        public string? VoucherCode { get; set; }
        public int? Discount { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
    }
}
