﻿using System;
using System.Collections.Generic;

namespace DemoRestApi.Models
{
    public partial class Shop
    {
        public Shop()
        {
            Orders = new HashSet<Order>();
            Products = new HashSet<Product>();
        }

        public int ShopId { get; set; }
        public string ShopName { get; set; } = null!;
        public string ShopAddress { get; set; } = null!;
        public string? ShopPhone { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
