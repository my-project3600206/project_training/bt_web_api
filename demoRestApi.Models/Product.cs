﻿using System;
using System.Collections.Generic;

namespace DemoRestApi.Models
{
    public partial class Product
    {
        public Product()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int ProductId { get; set; }
        public string ProductName { get; set; } = null!;
        public string? ProductDescription { get; set; }
        public int TypeId { get; set; }
        public double SellingPrice { get; set; }
        public double PromotionPrice { get; set; }
        public int StockAmount { get; set; }
        public string? ImageUrl { get; set; }
        public string? Color { get; set; }
        public int ShopId { get; set; }

        public virtual Shop Shop { get; set; } = null!;
        public virtual ProductType Type { get; set; } = null!;
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
