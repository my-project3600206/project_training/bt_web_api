﻿using System;
using System.Collections.Generic;

namespace DemoRestApi.Models
{
    public partial class Order
    {
        public Order()
        {
            OrderDetails = new HashSet<OrderDetail>();
        }

        public int OrderId { get; set; }
        public DateTime? OrderDate { get; set; }
        public DateTime? ShippingDate { get; set; }
        public int? OrderStatus { get; set; }
        public string? OrderCode { get; set; }
        public string PhoneNumber { get; set; } = null!;
        public string Address { get; set; } = null!;
        public int? PaymentStatus { get; set; }
        public int? PaymentMethod { get; set; }
        public DateTime? OverdueDate { get; set; }
        public int CustomerId { get; set; }
        public int ShopId { get; set; }
        public int? VoucherId { get; set; }

        public virtual Customer Customer { get; set; } = null!;
        public virtual Shop Shop { get; set; } = null!;
        public virtual Voucher? Voucher { get; set; }
        public virtual ICollection<OrderDetail> OrderDetails { get; set; }
    }
}
