﻿namespace DemoRestApi.Web.Responses
{
    public class OrderDetailRes : OrderRes
    {
       
        public List<ProductRes> Products {  get; set; }

        public OrderDetailRes()
        {
            Products = new List<ProductRes>();
        }

    }
    
}
