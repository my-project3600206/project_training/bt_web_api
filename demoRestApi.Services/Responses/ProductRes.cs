﻿namespace DemoRestApi.Web.Responses
{
    public class ProductRes
    {
        public int ProductId { get; set; }
        public string? ProductName { get; set; }
        public int? Quantity { get; set; }
        public double SellingPrice { get; set; }
        public double PromotionPrice { get; set; }
    }
}
