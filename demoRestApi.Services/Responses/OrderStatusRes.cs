﻿namespace DemoRestApi.Web.Responses
{
    public class OrderStatusRes
    {
        public OrderStatusRes()
        {
            this.orderIds = new List<int>();
            this.status = false;
            this.message = string.Empty;
        }

        public List<int> orderIds { get; set; }
        public bool status { get; set; }
        public string message { get; set; }

    }
}
