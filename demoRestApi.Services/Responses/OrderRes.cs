﻿namespace DemoRestApi.Web.Responses
{
    public class OrderRes
    {
        public int OrderId { get; set; }
        public string? OrderCode { get; set; }
        public double? TotalPrice { get; set; }
        public int? Quantity { get; set; }
        public string? ShopName { get; set; }
        public int? OrderStatus { get; set; }
        public DateTime? CreateDate { get; set; }
        public DateTime? OverdueDate { get; set; }
    }
}
