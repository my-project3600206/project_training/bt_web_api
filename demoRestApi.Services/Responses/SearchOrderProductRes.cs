﻿namespace DemoRestApi.Web.Responses
{
    public class SearchOrderProductRes
    {
        public int OrderID { get; set; }
        public string? OrderCode { get; set; }
        public double? TatolPrice { get; set; }
        public int? Quantiy { get; set; }
        public string? ShopName { get; set; }
        public string? ProductName { get; set; }
        public int? OrderStatus { get; set; }
        public DateTime? OverDueDate { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
