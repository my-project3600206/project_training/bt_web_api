﻿
using DemoRestApi.Web.Requests;
using DemoRestApi.Web.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoRestApi.Services
{
    public interface IOrderSevice
    {
        public List<OrderRes> GetAllOrders();
        public List<OrderDetailRes> GetOrderById(int orderId);
        public Task<OrderStatusRes> CreateOrder(OrderRequest orderRequest);
        public string DeleteProductOfOrder( int orderId, int productId);
        public Task UpdateCustomer();
        public List<SearchOrderProductRes> GetOrderBySearchProductKyword(string keyWord);
    }
}
