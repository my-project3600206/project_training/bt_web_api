﻿using DemoRestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoRestApi.Services.Implements
{
    public class UserSevice : IUserSevice
    {
        private readonly demoContext _context;

        public UserSevice(demoContext context)
        {
            _context = context;
        }


        public async Task<string> UpdateUserVip()
        {
            var userVip = (from o in _context.Orders
                          join od in _context.OrderDetails on o.OrderId equals od.OrderId
                          select new { o.CustomerId, od.Price, od.Quanity } into g
                          group g by g.CustomerId into temp
                          where temp.Count() > 10 && temp.Sum((x) => x.Price * x.Quanity) > 1000
                          select temp.Key).ToList();
            if(userVip == null || userVip.Count == 0)
            {
                _context.Dispose();
                return ("userVip not found");

            }
            foreach (var item in userVip)
            {
                var user = _context.Customers.Single((e) => e.CustomerId == item);
                if (user != null) 
                {
                    var vip = user.Vip;
                    if(vip == null || vip.ToUpper() != "VIP")
                    {
                        user.Vip = "VIP";
                        _context.Customers.Update(user);
                        _context.SaveChanges();
                    }
                }
            }
            return  ("update VIP for customer OK");

        }
    }
}
