﻿using DemoRestApi.Models;
using DemoRestApi.Services.Helper;
using DemoRestApi.Web.Requests;
using DemoRestApi.Web.Responses;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoRestApi.Services.Implements
{
    public class OrderSevice : IOrderSevice
    {
        private readonly demoContext _context;

        public OrderSevice(demoContext context)
        {
            _context = context;
        }
        private int getQuantity(List<ProductRequest> items, int productId)
        {
            var item = items.Find(match: x => x.ProductId == productId);
            if(item == null)
            {
                return 0;
            }
            return item.Quantity;
        }
        public async Task<OrderStatusRes> CreateOrder(OrderRequest orderRequest)
        {
            List<int> orderIds = new List<int>();
            var auto = new AutoIncrement();
            var productInfo = (from p in _context.Products
                               where orderRequest.OrderProducts.Select(x => x.ProductId).Contains(p.ProductId)
                               select new OrderProductRequest
                               {
                                   ShopId = p.ShopId,
                                   ProductId = p.ProductId,
                                   PromotionPrice = p.PromotionPrice,
                                   StockAmount = p.StockAmount,

                               }).ToList();

            var result = productInfo.GroupBy(n => n.ShopId)
                                    .Select(m =>
                                    {
                                        var items = new List<OrderProductRequest>();
                                        var products = m.Select((item) => item).ToList().Distinct();
                                        items.AddRange(products);
                                        return new
                                        {
                                            ShopId = m.Key,
                                            ProducIds = items
                                        };
                                    }).ToList();
            
                // tao nhieu order va nhieu orderDeail
            for (int i=0; i< result.Count; i++) 
            {
                var orderRequestItem = result[i];
                // add 1 order
                var orderId = auto.rederId();
                Console.WriteLine(orderId);
                orderIds.Add(orderId);
                
                Thread.Sleep(1000);
                var newOrder = new Order
                {
                    OrderId = orderId,
                    CustomerId = orderRequest.CustomerId,
                    PhoneNumber = orderRequest.Phonenumber,
                    Address = orderRequest.Address,
                    OrderDate = DateTime.Now,
                    OverdueDate = DateTime.Now,
                    OrderCode = auto.renderCode(),
                    ShopId = orderRequestItem.ShopId,
                    ShippingDate = DateTime.Now,
                    OrderStatus = 0,
                    PaymentMethod = 1,
                    PaymentStatus = 1,
                };
                _context.Orders.Add(newOrder);
                // find product
                foreach (var product in orderRequestItem.ProducIds)
                {
                    //add 1 orderDetail
                    int quantity = getQuantity(orderRequest.OrderProducts, product.ProductId);
                    if(quantity <= 0)
                    {
                        _context.Dispose();

                        return new OrderStatusRes { orderIds = new List<int>(), status = false, message = "Quantity invalid" };
                    }
                    Thread.Sleep(1000);
                    var newOrderDetail = new OrderDetail
                    {
                        OrderDetailId = auto.rederId(),
                        ProductId = product.ProductId,
                        OrderId  = orderId,
                        Price = (int)product.PromotionPrice,
                        Quanity = quantity

                    };
                    _context.OrderDetails.Add(newOrderDetail);
                    
                }
            }
            await _context.SaveChangesAsync();
            return new OrderStatusRes { orderIds = orderIds, status = true, message = "create order successfuly" };
                    
        }

       

        public string DeleteProductOfOrder(int orderId, int productId)
        {
            var findProduct = (from od in _context.OrderDetails
                               join o in _context.Orders on od.OrderId equals o.OrderId
                               where od.OrderId == orderId && od.ProductId == productId
                               select od).ToList();
            if (findProduct.Count == 0 )
            {
                _context.Dispose();
                return ("Produc no found");
            }
            else 
            {
                var findProductOfOrder = (from o in _context.Orders
                                          join od in _context.OrderDetails on o.OrderId equals od.OrderId
                                          where o.OrderId == orderId
                                          select o).ToList();
                if (findProductOfOrder.Count >1)
                {
                    _context.OrderDetails.RemoveRange(findProduct);
                    _context.SaveChangesAsync();
                    return ("delete product successfuly");
                }
                else
                {
                    _context.OrderDetails.RemoveRange(findProduct);
                    _context.Orders.RemoveRange(findProductOfOrder);
                    _context.SaveChangesAsync();
                    return ("delete order and product successfuly");
                }
               

            }
           
        }

        public List<OrderDetailRes> GetOrderById(int orderId)
        {
            List<OrderDetailRes> items = new List<OrderDetailRes> { };
            
            var orderInfo = (from o in _context.Orders
                             join s in _context.Shops on o.ShopId equals s.ShopId
                             where o.OrderId == orderId select new OrderDetailRes
                             {
                                 OrderId = o.OrderId,
                                 OrderCode = o.OrderCode,
                                 ShopName = s.ShopName,
                                 OrderStatus = o.OrderStatus,
                                 CreateDate = DateTime.Now,
                                 OverdueDate = DateTime.Now,
                             }).ToList();

            var products = (from o in _context.Orders
                            join od in _context.OrderDetails
                            on o.OrderId equals od.OrderId
                            join p in _context.Products on od.ProductId equals p.ProductId
                            where o.OrderId == orderId
                           select new ProductRes
                           {
                               ProductId = p.ProductId,
                               ProductName = p.ProductName,
                               PromotionPrice = (double)od.Price,
                               Quantity = od.Quanity,
                               SellingPrice = p.SellingPrice,
                              
                           }).ToList();
            if(orderInfo.Count == 1)
            {
                var item = orderInfo.First();
                item.Products = products;
                var sum = products.Sum((e)=> e.PromotionPrice * e.Quantity);
                item.TotalPrice = sum;

                item.Quantity = products.Sum((e) => e.Quantity); // products.Count;
                items.Add(item);
                return items;
            }
            return items;
        }
        public  List<OrderRes> GetAllOrders()
        {
            var orderRes = (from o in _context.Orders
                            join od in _context.OrderDetails
                            on o.OrderId equals od.OrderId
                            join s in _context.Shops on o.ShopId equals s.ShopId
                            group new {o, od, s }  by new { od.OrderId, s.ShopName, o.OverdueDate, o.OrderStatus, o.OrderCode }
                            into groupItems
                            select new OrderRes
                            {
                                OrderId = groupItems.Key.OrderId,
                                OrderCode = groupItems.Key.OrderCode,
                                ShopName = groupItems.Key.ShopName,
                                TotalPrice = groupItems.Sum(a => a.od.Quanity * a.od.Price),
                                Quantity = groupItems.Sum(a => a.od.Quanity),
                                OrderStatus = groupItems.Key.OrderStatus,
                                CreateDate = DateTime.Now,
                                OverdueDate = DateTime.Now,
                            }).ToList();
            
            return orderRes;


           
        }

        public List<SearchOrderProductRes> GetOrderBySearchProductKyword(string keyWord)
        {
            var searchOreder = (from od in _context.OrderDetails
                                join o in _context.Orders on od.OrderId equals o.OrderId
                                join p in _context.Products on od.ProductId equals p.ProductId
                                join s in _context.Shops on o.ShopId equals s.ShopId
                                where p.ProductName.Contains(keyWord)
                                select new SearchOrderProductRes
                                {
                                    OrderID = o.OrderId,
                                    OrderCode = o.OrderCode,
                                    ShopName = s.ShopName,
                                    ProductName = p.ProductName,
                                    TatolPrice = od.Price * od.Quanity,
                                    Quantiy = od.Quanity,
                                    OverDueDate = o.OverdueDate,
                                    CreateDate = o.OrderDate

                                }).ToList();
            return searchOreder;
        }

        public Task UpdateCustomer()
        {
            throw new NotImplementedException();
        }
    }

}
