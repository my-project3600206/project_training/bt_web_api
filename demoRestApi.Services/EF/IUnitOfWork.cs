﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoRestApi.Services.EF
{
    public interface IUnitOfWork
    {
        IOrderSevice orderService { get; }
        IUserSevice userService { get; }
        void Save();
    }
}
