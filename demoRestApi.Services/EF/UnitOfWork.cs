﻿using DemoRestApi.Models;
using DemoRestApi.Services.Implements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoRestApi.Services.EF
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly demoContext _context;

        public IOrderSevice orderService {  get; }
        public IUserSevice userService { get; }
        public UnitOfWork(demoContext context)
        {
            _context = context;
            orderService = new OrderSevice(_context);
            userService = new UserSevice(_context);
        }
        public void Dispose()
        {
            _context.Dispose();
        }
        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
