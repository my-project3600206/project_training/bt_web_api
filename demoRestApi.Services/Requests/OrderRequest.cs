﻿namespace DemoRestApi.Web.Requests
{
    public class OrderRequest
    {
        public int CustomerId { get; set; }
        public string Address {  get; set; }
        public string Phonenumber { get; set; }
        public List<ProductRequest> OrderProducts { get; set; }

        public OrderRequest()
        {
            this.CustomerId = 0;
            this.Address = string.Empty;
            this.Phonenumber = string.Empty;
            this.OrderProducts = new List<ProductRequest>();
        }
    }
}
