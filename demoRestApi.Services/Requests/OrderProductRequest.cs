﻿namespace DemoRestApi.Web.Requests
{
    public class OrderProductRequest
    {
        public int ProductId { get; set; }
        public int ShopId { get; set; }
        public int Quantity { get; set; }
        public int StockAmount { get; set; }
        public double PromotionPrice { get; set; }
        
    }
    public class ProductRequest
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
