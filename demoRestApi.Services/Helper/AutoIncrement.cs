﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoRestApi.Services.Helper
{
    public class AutoIncrement
    {
        private Random random = new Random();
        public AutoIncrement()
        {

        }
        public int rederId()
        {
            return GetCurrentTimestamp();
        }
        private int GetCurrentTimestamp()
        {
            // Thời gian hiện tại theo múi giờ UTC
            DateTime currentTimeUtc = DateTime.UtcNow;

            // Điểm thời gian Epoch (1/1/1970)
            DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            // Chuyển đổi thời gian hiện tại thành số giây từ Epoch
            int timestamp = (int)(currentTimeUtc - epoch).TotalSeconds;

            return timestamp;
        }
        public string renderCode(int length = 8)
        {

            const string chars = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());

        }
    }
}
