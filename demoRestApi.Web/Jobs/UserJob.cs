﻿using DemoRestApi.Services.EF;
using Quartz;

namespace DemoRestApi.Web.Jops
{
    public class UserJob : IJob
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserJob(IUnitOfWork unitOfWork) 
        {
            _unitOfWork = unitOfWork;
        }
        public Task Execute(IJobExecutionContext context)
        {
            string message = "This job will run again at: " + context.NextFireTimeUtc.ToString();
            Console.WriteLine(message);
            _unitOfWork.userService.UpdateUserVip();
            return Task.CompletedTask;
           
        }
    }
}
