﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.Extensions.Primitives;

namespace DemoRestApi.Web.Filters
{
    public class HeaderFilter : ActionFilterAttribute
    {
        private readonly string _headerName;
        string value = "123456";

        public HeaderFilter(string headerName)
        {
            _headerName = headerName;
        }
        override
        public async void OnActionExecuting(ActionExecutingContext context)
        {
            
            if(context.HttpContext.Request.Headers.TryGetValue(_headerName, out var headerValue)) 
            {
                
                // You can access the header value and perform any logic here
                if (headerValue != value)
                {
                    context.Result = new ObjectResult($" header {_headerName} is missing")
                    {
                        StatusCode = StatusCodes.Status403Forbidden   // Return a 403 Forbidden status code
                    };
                }

                else
                {
                    context.ActionArguments[_headerName] = headerValue;
                    

                }
            }
            else
            {
                // Handle the case when the header is not present
                context.Result = new ObjectResult($"Header {_headerName} is missing.")
                {
                    StatusCode = 403 // Return a 403 Forbidden status code
                };
            }

            base.OnActionExecuting(context);
            
        }

    }
}
