﻿using AutoMapper;
using DemoRestApi.Models;
using DemoRestApi.Services;
using DemoRestApi.Services.EF;
using DemoRestApi.Web.Controllers.Api;
using DemoRestApi.Web.Filters;
using DemoRestApi.Web.Requests;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Concurrent;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;

namespace DemoRestApi.Web.Controllers
{
    [ApiController]
    [Route("api/v1")]
    public class OrderController : BaseApiController
    {
      
        private readonly IUnitOfWork _unitOfWork;
    
        public OrderController(IUnitOfWork unitOfWork, IMapper mapper) : base(mapper)
        {
            _unitOfWork = unitOfWork;
            
        }
        
        [HttpGet]
        [Route("orders")]
        
        public IActionResult GetAllOrder()
        {
            try
            {
                var getAllOrder = (_unitOfWork.orderService.GetAllOrders());
                return Ok(getAllOrder);
            }
            catch (Exception ex) 
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal server error" + ex.Message);
            }
        }
        [HeaderFilter("secret_key")]
        [HttpGet]
        [Route("orders/{orderId}")]
        public IActionResult GetOrderById(int orderId)
        {
            try
            {
                var getOrderById = _unitOfWork.orderService.GetOrderById(orderId);
                return getOrderById == null ? NotFound() : Ok(getOrderById);
                
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal server error" + ex.Message);
            }
        }

        [HttpPost]
        [Route("orders")]
        public async Task<ActionResult<object>> CreateOrder([FromBody] OrderRequest orderRequest) 
        {
            try
            {
                
                var createOrder = await _unitOfWork.orderService.CreateOrder(orderRequest);
                return Ok(createOrder);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal server error" + ex.Message);
            }

        }

        [HttpGet]
        [Route("orders/search-product")]
        public IActionResult GetOrderBySearchProductKeyWord(string keyWord) 
        {
            try
            {
                var search = (_unitOfWork.orderService.GetOrderBySearchProductKyword(keyWord));
                return Ok(search);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal server error" + ex.Message);
            }
        }
        
        [HttpDelete]
        [Route("orders/{orderId}/product/{productId}")]
        public IActionResult DeleteProductOfOrder( int orderId, int productId) 
        {
            try
            {
                var deleteOrder = _unitOfWork.orderService.DeleteProductOfOrder(orderId, productId);
                return Ok(deleteOrder);
            }
            catch (Exception ex) 
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal server error" + ex.Message);
            }
        }

    }
}
