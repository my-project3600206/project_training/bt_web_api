﻿using DemoRestApi.Services.EF;
using Microsoft.AspNetCore.Mvc;

namespace DemoRestApi.Web.Controllers
{
    public class UserController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public UserController(IUnitOfWork unitOfWork) 
        {
            _unitOfWork = unitOfWork;
        }
        public async Task<IActionResult> UpdateUserVip()
        {
            try
            {
                await _unitOfWork.userService.UpdateUserVip();
                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Internal server error" + ex.Message);
            }
           
        }
    }
}
