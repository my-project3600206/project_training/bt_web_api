﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace DemoRestApi.Web.Controllers.Api
{
    public class BaseApiController : Controller
    {
        protected IMapper Mapper { get; }

        public BaseApiController(IMapper mapper)
        {
            this.Mapper = mapper;
        }
    }
}
