using DemoRestApi.Models;
using DemoRestApi.Services.Implements;
using DemoRestApi.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using DemoRestApi.Services.EF;
using Quartz;
using DemoRestApi.Web.Jops;
using DemoRestApi.Web.Filters;

var builder = WebApplication.CreateBuilder(args);

// 1.Add services to the container.
IServiceCollection services = builder.Services;
/*builder.Services.AddControllers(option =>
{
    
});*/

builder.Services.AddControllers();
//Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddDbContext<demoContext>(option => option.UseSqlServer(builder.Configuration.GetConnectionString("demo")));
// cau hinh swaggeer
services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1", Description = "Version: 1.0.0" });
});

// cau hinh loggin
builder.Services.AddHttpLogging(loggin =>
{
    loggin.LoggingFields = Microsoft.AspNetCore.HttpLogging.HttpLoggingFields.All;
});



// config schedule
builder.Services.AddQuartz(q =>
{
    q.UseMicrosoftDependencyInjectionJobFactory();
    var jopKey = new JobKey("UserJob");
    q.AddJob<UserJob>(opts => opts.WithIdentity(jopKey));
    q.AddTrigger(opts => opts
                    .ForJob(jopKey)
                    .WithIdentity("UserJob-trigger")
                    .WithCronSchedule("0/5 * * * * ?")  // run again 5s
                    //.WithCronSchedule("0 0 0/1 * * ?") // run again 1h
                    ); 
});

builder.Services.AddQuartzHostedService(q => q.WaitForJobsToComplete = true);
services.AddRouting();

services.AddAutoMapper(typeof(Program));
services.AddTransient<IUnitOfWork, UnitOfWork>();
services.AddTransient<IOrderSevice, OrderSevice>();
services.AddTransient<IUserSevice, UserSevice>();
var app = builder.Build();
app.UseHttpLogging();

//2. Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI(c =>
{
    c.SwaggerEndpoint("/swagger/v1/swagger.json", "V1 Docs");
    c.RoutePrefix = "DocumentAPI";
});

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseRouting();
app.UseHttpsRedirection();
app.UseAuthorization();

app.MapControllers();
app.Run();
